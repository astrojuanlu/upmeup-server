/* eslint-disable prettier/prettier */
import { Field, InputType } from '@nestjs/graphql';
import * as mongoose from 'mongoose';
import { RequiredExperience } from 'src/common/types/requiredExpirience';
import { Competencies } from 'src/competencies/models/competence';
import { OfferSchema } from '../models/company-offer';
import { OfferStatus } from 'src/common/types/offerStatus';

export type CompanyOfferDocument = CreateOfferDto & mongoose.Document;

@InputType({ description: 'Create new offer' })
export class CreateOfferDto {  
    @Field()
    userId: string;
  
    @Field()
    title: string;

    @Field()
    eduLevel: string;

    @Field()
    city: string;
  
    @Field()
    salaryRange: string;
  
    @Field()
    remote: string;
  
    @Field()
    contractType: string;
  
    @Field()
    workHours: string;
  
    @Field(() => [Competencies])
    competencies: Competencies[];
    
    @Field(() => Number)
    enrolled: number;

    @Field()
    description: string;
    
    @Field()
    requirements: string;

    @Field(() => Date)
    createdDate: Date;

    @Field()
    user: string;

    @Field(() => RequiredExperience, { nullable: true })
    requiredExperience: RequiredExperience;  

    @Field(() => OfferStatus, { nullable: true })
    status: OfferStatus;  

}