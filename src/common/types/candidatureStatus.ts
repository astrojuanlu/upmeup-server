export enum CandidatureStatus {
    HIRED,
    PRESELECTED,
    REJECTED
}