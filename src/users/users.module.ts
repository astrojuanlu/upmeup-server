import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersResolver } from './users.resolver';
import { User, UserSchema } from './models/user';
import { StorageService } from 'src/common/modules/s3storage/sotrageService';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
  ],
  providers: [UsersService, UsersResolver, StorageService],
  exports: [UsersService]
})
export class UsersModule {}
