/* eslint-disable prettier/prettier */
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types as MongooseTypes } from 'mongoose';
import { Competencies } from 'src/competencies/models/competence';
import { Sector } from 'src/sectors/models/sector';
import { Softskill } from 'src/softskills/models/softskills';

@ObjectType()
export class Preferences {

  @Field(() => String, {nullable: true})
  @Prop()
  language: String;

} 

@Schema()
@ObjectType({ description: 'from UserModel (src/users/models/user.ts)' })
export class User {
  @Field(() => ID)
  _id: MongooseTypes.ObjectId;

  @Field()
  @Prop()
  name: string;

  @Field()
  @Prop()
  surname: string;

  @Field({ nullable: true })
  @Prop()
  legalForm: string;

  @Field()
  @Prop()
  city: string;

  @Field(type => [Sector])
  @Prop()
  sector: Sector[];
  
  @Field()
  @Prop()
  eduLevel: string;

  @Prop()
  password: string;

  @Field()
  @Prop()
  type: string;

  @Field()
  @Prop()
  email: string;

  @Field({ nullable: true })
  @Prop()
  website: string;

  @Field()
  @Prop()
  jobPosition: string;
  
  @Field()
  @Prop()
  lastJobTasks: string;
  
  @Field()
  @Prop()
  experience: string;

  @Field(() => [String])
  @Prop()
  languages: string[];

  @Field(() => [Competencies], { nullable: 'itemsAndList' })
  @Prop()
  competencies: Competencies[];

  @Field(() => [Softskill])
  @Prop()
  softSkills: Softskill[];

  @Field(() => Preferences, {nullable: true} )
  @Prop()
  preferences: Preferences;

  @Field({nullable: true})
  @Prop()
  avatarB64: string;

  @Field({nullable: true})
  @Prop()
  video: string;

  @Field({nullable: true})
  @Prop()
  cv: string;

  @Field({nullable: true})
  @Prop()
  coverLetter: string;
  
  @Prop()
  token: string;

  @Prop()
  token_expiry: Date;

  @Field()
  @Prop()
  commsOK: Boolean;

  @Field()
  @Prop()
  privacyPolicyOK: Boolean;
  

}
export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);

